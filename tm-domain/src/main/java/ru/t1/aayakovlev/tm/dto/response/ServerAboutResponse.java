package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    @NotNull
    String gitCommitTime;

    @NotNull
    private String email;

    @NotNull
    private String name;

    @NotNull
    private String gitBranch;

    @NotNull
    private String gitCommitId;

    @NotNull
    private String gitCommitterName;

    @NotNull
    private String gitCommitterEmail;

    @NotNull
    private String gitCommitMessage;

}
