package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.TaskShowAllRequest;
import ru.t1.aayakovlev.tm.dto.response.TaskShowAllResponse;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskShowAllCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show all tasks.";

    @NotNull
    public static final String NAME = "task-show-all";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW ALL TASKS]");
        System.out.println("Sorts to enter:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final TaskShowAllRequest request = new TaskShowAllRequest(getToken());
        request.setSort(sort);
        @Nullable final TaskShowAllResponse response = getTaskEndpoint().showAll(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        @NotNull final List<TaskDTO> tasks = response.getTasks();

        renderTasks(tasks);
    }

}
