package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.UserServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class UserServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private final UserRepository userRepository = connectionService.getSqlSession().getMapper(UserRepository.class);

    @NotNull
    private static final UserService service = new UserServiceImpl(propertyService, connectionService);

    @BeforeClass
    public static void init() throws EntityEmptyException {
        service.save(COMMON_USER_ONE);
        service.save(COMMON_USER_TWO);
    }

    @AfterClass
    public static void after() {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnUser() throws AbstractException {
        @Nullable final UserDTO user = service.findById(COMMON_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(COMMON_USER_ONE.getEmail(), user.getEmail());
        Assert.assertEquals(COMMON_USER_ONE.getFirstName(), user.getFirstName());
        Assert.assertEquals(COMMON_USER_ONE.getLastName(), user.getLastName());
        Assert.assertEquals(COMMON_USER_ONE.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(COMMON_USER_ONE.getLogin(), user.getLogin());
    }

    @Test
    public void When_FindByIdNotExistsUser_Expect_ReturnNull() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.findById(USER_ID_NOT_EXISTED));
    }

    @Test
    public void When_SaveNotNullUser_Expect_ReturnUser() {
        @NotNull final UserDTO savedUser = service.save(ADMIN_USER_ONE);
        Assert.assertNotNull(savedUser);
        Assert.assertEquals(ADMIN_USER_ONE, savedUser);
        @Nullable final UserDTO user = userRepository.findById(ADMIN_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER_ONE, user);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListUsers() {
        final List<UserDTO> users = service.findAll();
        Assert.assertArrayEquals(users.toArray(), COMMON_USER_LIST.toArray());
    }

    @Test
    public void When_RemoveExistedUser_Expect_ReturnUser() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE));
        service.removeById(ADMIN_USER_ONE.getId());
    }

    @Test
    public void When_RemoveAll_Expect_NullUsers() {
        userRepository.save(ADMIN_USER_ONE);
        userRepository.save(ADMIN_USER_TWO);
        userRepository.clear();
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(ADMIN_USER_ONE.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(ADMIN_USER_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdExistedUser_Expect_User() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE));
        service.removeById(ADMIN_USER_ONE.getId());
    }

    @Test
    public void When_RemoveByIdNotExistedUser_Expect_ThrowsEntityNotFoundException() throws AbstractException {
         service.removeById(USER_ID_NOT_EXISTED);
    }

    @Test
    public void When_FindByLogin_Expect_User() throws AbstractException {
        Assert.assertNotNull(service.findByLogin(COMMON_USER_ONE.getLogin()));
    }

    @Test
    public void When_FindByEmail_Expect_User() throws AbstractException {
        Assert.assertNotNull(service.findByEmail(COMMON_USER_ONE.getEmail()));
    }

    @Test
    public void When_IsLoginExists_Expect_True() throws LoginEmptyException {
        Assert.assertTrue(service.isLoginExists(COMMON_USER_ONE.getLogin()));
    }

    @Test
    public void When_IsEmailExist_Expect_True() throws EmailEmptyException {
        Assert.assertTrue(service.isEmailExists(COMMON_USER_ONE.getEmail()));
    }

    @Test
    public void When_LockUserByLogin_Expect_LockedUser() throws AbstractException {
        service.save(ADMIN_USER_ONE);
        @NotNull final UserDTO user = service.lockUserByLogin(ADMIN_USER_ONE.getLogin());
        Assert.assertTrue(user.isLocked());
    }

    @Test
    public void When_RemoveByLogin_Expect_RemovedUser() throws AbstractException {
        service.save(ADMIN_USER_TWO);
        service.removeByLogin(ADMIN_USER_TWO.getLogin());

    }

    @Test
    public void When_RemoveByEmail_Expect_RemovedUser() throws AbstractException {
        service.save(ADMIN_USER_TWO);
        service.removeByEmail(ADMIN_USER_TWO.getEmail());
    }

    @Test
    public void When_SetPassword_Expect_ChangedPassword() throws AbstractException {
        @NotNull final UserDTO user = service.setPassword(COMMON_USER_ONE.getId(), PASSWORD);
        Assert.assertEquals(PASSWORD_HASH, user.getPasswordHash());
    }

    @Test
    public void When_UnlockUserByLogin_Expect_UnlockedUser() throws AbstractException {
        service.save(ADMIN_USER_ONE);
        service.lockUserByLogin(ADMIN_USER_ONE.getLogin());
        @NotNull final UserDTO user = service.unlockUserByLogin(ADMIN_USER_ONE.getLogin());
        Assert.assertFalse(user.isLocked());
    }

    @Test
    public void When_UpdateUser_Expect_UpdatedUser() throws AbstractException {
        service.save(ADMIN_USER_ONE);
        @NotNull final UserDTO user = service.updateUser(ADMIN_USER_ONE.getId(), FIRST_NAME, LAST_NAME, MIDDLE_NAME);
        Assert.assertEquals(FIRST_NAME, user.getFirstName());
        Assert.assertEquals(LAST_NAME, user.getLastName());
        Assert.assertEquals(MIDDLE_NAME, user.getMiddleName());
    }

}
