package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.SessionServiceImpl;

import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class SessionServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final SessionService service = new SessionServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws EntityEmptyException {
        service.save(SESSION_USER_ONE);
        service.save(SESSION_USER_TWO);
    }

    @AfterClass
    public static void after() {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ReturnSession() throws AbstractException {
        @Nullable final SessionDTO session = service.findById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_USER_ONE.getUserId(), session.getUserId());
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.findById(USER_ID_NOT_EXISTED, SESSION_ID_NOT_EXISTED)
        );

    }

    @Test
    public void When_SaveNotNullSESSION_Expect_ReturnSession() throws AbstractException {
        @NotNull final SessionDTO savedSession = service.save(SESSION_ADMIN_ONE);
        Assert.assertNotNull(savedSession);
        Assert.assertEquals(SESSION_ADMIN_ONE, savedSession);
        @Nullable final SessionDTO session = service.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_ADMIN_ONE, session);
    }

    @Test
    public void When_CountCommonUserSessions_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedSession_Expected_ReturnTrue() throws AbstractException {
        final boolean exists = service.existsById(SESSION_USER_ONE.getUserId(), SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedSession_Expected_ReturnFalse() throws AbstractException {
        final boolean exists = service.existsById(USER_ID_NOT_EXISTED, SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedSession_Expected_ReturnTrue() throws AbstractException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedSession_Expected_ReturnFalse() throws AbstractException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_RemoveExistedSession_Expect_ReturnSession() throws AbstractException {
        Assert.assertNotNull(service.save(SESSION_ADMIN_TWO));
        service.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveNotSession_Expect_ThrowsEntityNotFoundException() throws AbstractException {
         service.removeById(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED.getId());
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountSessions() throws AbstractException {
        service.save(SESSION_ADMIN_ONE);
        service.save(SESSION_ADMIN_TWO);
        service.clear(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, service.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedSession_Expect_Session() throws AbstractException {
        Assert.assertNotNull(service.save(SESSION_ADMIN_TWO));
        service.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO.getId());
    }

}
