package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.TaskService;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.TaskServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_ID_NOT_EXISTED;
import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class TaskRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private static final TaskRepository repository = sqlSession.getMapper(TaskRepository.class);

    @NotNull
    private static final TaskService service = new TaskServiceImpl(connectionService);

    @Before
    public void init() throws EntityEmptyException {
        service.save(TASK_USER_ONE);
        service.save(TASK_USER_TWO);
    }

    @After
    public void after() {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsTask_Expect_ReturnTask() {
        @Nullable final TaskDTO task = repository.findById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_USER_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_USER_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_USER_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_USER_ONE.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_USER_ONE.getCreated(), task.getCreated());
    }

    @Test
    public void When_FindByIdExistsTask_Expect_ReturnNull() {
        @Nullable final TaskDTO task = repository.findById(USER_ID_NOT_EXISTED, TASK_ID_NOT_EXISTED);
        Assert.assertNull(task);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullTask_Expect_ReturnTask() {
        repository.save(TASK_ADMIN_ONE);
        sqlSession.commit();
        @Nullable final TaskDTO task = repository.findById(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_ADMIN_ONE.getId(), task.getId());
    }

    @Test
    public void When_CountCommonUserTasks_Expect_ReturnTwo() {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListTasks() {
        final List<TaskDTO> tasks = repository.findAllUserId(COMMON_USER_ONE.getId());
        for (int i = 0; i < tasks.size(); i++) {
            Assert.assertEquals(USER_TASK_LIST.get(i).getName(), tasks.get(i).getName());
        }
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedTaskList() {
        final List<TaskDTO> tasks = repository.findAllSorted(COMMON_USER_ONE.getId(), "name");
        for (int i = 0; i < tasks.size(); i++) {
            Assert.assertEquals(USER_TASK_SORTED_LIST.get(i).getName(), tasks.get(i).getName());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedTask_Expect_ReturnTask() {
        repository.save(TASK_ADMIN_TWO);
        sqlSession.commit();
        repository.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
        sqlSession.commit();
    }

    @Test
    public void When_RemoveNotTask_Expect_ThrowsEntityNotFoundException() {
        repository.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId());
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_ZeroCountTasks() {
        repository.save(TASK_ADMIN_ONE);
        sqlSession.commit();
        repository.save(TASK_ADMIN_TWO);
        sqlSession.commit();
        repository.clear(ADMIN_USER_ONE.getId());
        sqlSession.commit();
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedTask_Expect_Task() {
        repository.save(TASK_ADMIN_TWO);
        sqlSession.commit();
        repository.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
        sqlSession.commit();
    }

    @Test
    public void When_RemoveByIdNotExistedTask_Expect_ThrowsEntityNotFoundException() {
        repository.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId());
    }

    @Test
    public void When_FindAllByProjectIdExistedProject_Expect_ReturnTaskList() {
        final List<TaskDTO> tasks = repository.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertNotEquals(0, tasks.size());
    }

    @Test
    public void When_FindAllByProjectIdNotExistedProject_Expect_ReturnEmptyList() {
        final List<TaskDTO> tasks = repository.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_ID_NOT_EXISTED);
        Assert.assertEquals(0, tasks.size());
    }

}
