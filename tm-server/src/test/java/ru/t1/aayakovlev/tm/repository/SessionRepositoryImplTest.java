package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.SessionService;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.SessionServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class SessionRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private static final SessionRepository repository = sqlSession.getMapper(SessionRepository.class);

    @NotNull
    private static final SessionService service = new SessionServiceImpl(connectionService);

    @Before
    public void init() throws EntityEmptyException {
        service.save(SESSION_USER_ONE);
        service.save(SESSION_USER_TWO);
    }

    @After
    public void after() {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsSession_Expect_ReturnProject() {
        final @Nullable SessionDTO project = repository.findById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(SESSION_USER_ONE.getUserId(), project.getUserId());
    }

    @Test
    public void When_FindByIdExistsSession_Expect_ReturnNull() {
        @Nullable final SessionDTO session = repository.findById(USER_ID_NOT_EXISTED, SESSION_ID_NOT_EXISTED);
        Assert.assertNull(session);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullSession_Expect_ReturnProject() {
        repository.save(SESSION_ADMIN_ONE);
        sqlSession.commit();
        @Nullable final SessionDTO session = repository.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_ADMIN_ONE.getUserId(), session.getUserId());
    }

    @Test
    public void When_CountCommonUserSessions_Expect_ReturnTwo() {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListSessions() {
        final List<SessionDTO> sessions = repository.findAllUserId(COMMON_USER_ONE.getId());
        for (int i = 0; i < sessions.size(); i++) {
            Assert.assertEquals(USER_SESSION_LIST.get(i).getId(), sessions.get(i).getId());
        }
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedSessionList() {
        final List<SessionDTO> sessions = repository.findAllSorted(COMMON_USER_ONE.getId(), "id");
        for (int i = 0; i < sessions.size(); i++) {
            Assert.assertEquals(USER_SESSION_SORTED_LIST.get(i).getId(), sessions.get(i).getId());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedSession_Expect_ReturnSession() {
        repository.save(SESSION_ADMIN_THREE);
        sqlSession.commit();
        repository.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_THREE.getId());
        sqlSession.commit();
    }

    @Test
    public void When_RemoveNotSession_Expect_ThrowsEntityNotFoundException() {
        repository.removeById(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED.getId());
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_ZeroCountSessions() {
        repository.clear(ADMIN_USER_ONE.getId());
        sqlSession.commit();
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedSession_Expect_Session() {
        repository.save(SESSION_ADMIN_TWO);
        sqlSession.commit();
        repository.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO.getId());
        sqlSession.commit();
    }

    @Test
    public void When_RemoveByIdNotExistedSession_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED.getId())
        );
    }

}
