package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;

import java.util.List;

public interface SessionService {

    void clear();

    void clear(@Nullable final String userId) throws UserIdEmptyException;

    int count(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<SessionDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException;

    @NotNull
    SessionDTO findById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    SessionDTO save(@NotNull final SessionDTO model);

    boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

}
