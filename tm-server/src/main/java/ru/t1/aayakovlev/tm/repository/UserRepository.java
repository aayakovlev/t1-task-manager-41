package ru.t1.aayakovlev.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

import java.util.List;

public interface UserRepository {

    @Delete("DELETE FROM task_manager.tm_user")
    void clear();

    @Select("SELECT COUNT(id) FROM task_manager.tm_user")
    int count();

    @Nullable
    @Select("SELECT * FROM task_manager.tm_user")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<UserDTO> findAll();

    @Nullable
    @Select("SELECT * FROM task_manager.tm_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findById(@NotNull @Param("id") final String id);

    @Nullable
    @Select("SELECT * FROM task_manager.tm_user WHERE email = #{email}")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByEmail(@NotNull @Param("email") final String email);

    @Nullable
    @Select("SELECT * FROM task_manager.tm_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByLogin(@NotNull @Param("login") final String login);

    @Delete("DELETE FROM task_manager.tm_user WHERE id = #{id}")
    void removeById(@NotNull @Param("id") final String id);

    @Insert("INSERT INTO task_manager.tm_user (id, login, password, email, first_name, last_name, middle_name, role, locked) " +
            "VALUES " +
            "(#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})"
    )
    void save(@NotNull final UserDTO user);

    @Update("UPDATE task_manager.tm_user SET " +
            "login = #{login}, password = #{passwordHash}, email = #{email}, first_name = #{firstName}, " +
            "last_name = #{lastName}, middle_name = #{middleName}, role = #{role}, locked = #{locked} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull final UserDTO user);

}
