package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

public interface AuthService {

    @NotNull
    SessionDTO validateToken(@Nullable final String token);

    void invalidate(@Nullable final SessionDTO session);

    @NotNull
    String login(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException;

    @NotNull
    UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException;

    @NotNull
    UserDTO profile(@Nullable final String userId) throws AbstractException;

}
