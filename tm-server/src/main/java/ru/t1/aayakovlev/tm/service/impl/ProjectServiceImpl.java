package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectServiceImpl implements ProjectService {

    @NotNull
    private final ConnectionService connectionService;

    public ProjectServiceImpl(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        if (comparator == NameComparator.INSTANCE) return "name";
        return "id";
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final ProjectDTO project = findById(userId, id);
        project.setStatus(status);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.update(project);
            session.commit();
            return project;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        return save(project);
    }


    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        project.setUserId(userId);
        return save(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.update(project);
            session.commit();
            return project;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable final List<ProjectDTO> projects) {
        clear();
        if (projects == null || projects.size() == 0) return;
        for (@NotNull final ProjectDTO project : projects) {
            save(project);
        }
    }

    @NotNull
    public ProjectDTO save(@NotNull final ProjectDTO project) {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.save(project);
            session.commit();
            return project;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.clearAll();
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.clear(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public int count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            return repository.count(userId);
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            @Nullable final List<ProjectDTO> projects;
            if (sort == null) {
                projects = repository.findAllUserId(userId);
            } else {
                projects = repository.findAllSorted(userId, getSortType(sort.getComparator()));
            }
            if (projects == null) return new ArrayList<>();
            return projects;
        }
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            @Nullable final List<ProjectDTO> projects = repository.findAll();
            if (projects == null) return new ArrayList<>();
            return projects;
        }
    }

    @NotNull
    @Override
    public ProjectDTO findById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            @Nullable final ProjectDTO project = repository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(id);
        project.setUserId(userId);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.removeById(userId, id);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
