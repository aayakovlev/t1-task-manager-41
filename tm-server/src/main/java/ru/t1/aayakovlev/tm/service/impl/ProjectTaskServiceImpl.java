package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;

import java.util.List;

public final class ProjectTaskServiceImpl implements ProjectTaskService {

    @NotNull
    private final ConnectionService connectionService;

    public ProjectTaskServiceImpl(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private ProjectRepository getProjectRepository(@NotNull final SqlSession session) {
        return session.getMapper(ProjectRepository.class);
    }

    @NotNull
    private TaskRepository getTaskRepository(@NotNull final SqlSession session) {
        return session.getMapper(TaskRepository.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(session);
            if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(session);
            @Nullable final TaskDTO task = taskRepository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            session.commit();
            return task;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(session);
            @Nullable final ProjectDTO project = projectRepository.findById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(session);
            @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
            if (tasks != null) {
                for (@NotNull final TaskDTO task : tasks) taskRepository.removeById(userId, task.getId());
            }
            projectRepository.removeById(userId, projectId);
            session.commit();
            return project;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(session);
            if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(session);
            @Nullable final TaskDTO task = taskRepository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            taskRepository.update(task);
            session.commit();
            return task;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
